% JPLSINFO(1) jplstran @JLST_VERSION@
% Mathieu Malaterre
% @JLST_DATE@

# NAME

jplstran – lossless transformation for JPEG-LS files

# SYNOPSIS

**jplstran**␣*input.jls*␣*output.jls*

# DESCRIPTION

**jplstran** Execute lossless transformation on JPEG-LS file. In particular it will not change image buffer where NEAR != 0

# OPTIONS

**-h**, **--help**
:   Display a friendly help message.

**--version**
:   Display the current version of charls-tools as well as the underlying charls version used.

**-i**, **--input**
:   Specify the input file(s) to read.

**-o**, **--output**
:   Specify the output file(s) to write.

**--crop**
:   Crop WxH+X+Y

**--flip** horizontal
:   Mirror image horizontally (left-right).

**--flip** vertical
:   Mirror image vertically (top-bottom).

**--rotate** 90
:   Rotate image 90 degrees clockwise.

**--rotate** 180
:   Rotate image 180 degrees.

**--rotate** 270
:   Rotate image 270 degrees clockwise (or 90 ccw).

**--transpose**
:   Transpose image (across UL-to-LR axis).

**--transverse**
:   Transverse transpose (across UR-to-LL axis).

**--wipe**
:   Wipe WxH+X+Y

# COPYRIGHT

BSD-3-Clause
